## 1.0.0
ohos 正式版
## 0.0.2-SNAPSHOT
ohos 第2个版本
 * 更新sdk6
## 0.0.1-SNAPSHOT
ohos 第一个版本
 * 实现了原库的大部分 api
 * 因为Camera未提供自动缩放，自动缩放未实现
 * 因为Matrix无法动态修改，扫描样式雷达、网格未实现
 * 因为ImagePicker开源库未支持裁剪、设置相机文本，裁剪、设置相机文本未实现