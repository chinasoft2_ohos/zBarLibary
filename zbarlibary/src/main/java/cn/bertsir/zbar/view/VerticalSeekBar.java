package cn.bertsir.zbar.view;


import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.render.Canvas;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

/**
 * Created by Bert on 2019/3/1.
 * Mail: bertsir@163.com
 */
public class VerticalSeekBar extends Slider implements Component.DrawTask{

    public static final int ROTATION_ANGLE_CW_90 = 90;
    public static final int ROTATION_ANGLE_CW_270 = 270;

    private int mRotationAngle = 90;

    public VerticalSeekBar(Context context) {
        super(context);//注意是super 而不是调用其他构造函数
    }

    public VerticalSeekBar(Context context, AttrSet attrs) {
        super(context, attrs);
        initialize(attrs);
    }

    public VerticalSeekBar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    private void initialize(AttrSet attrs) {

//        if (attrs != null) {
//            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.VerticalSeekBar, defStyleAttr, defStyleRes);
//            final int rotationAngle = a.getInteger(R.styleable.VerticalSeekBar_seekBarRotation, 0);
        try {
            int rotationAngle=attrs.getAttr("seekBarRotation").get().getIntegerValue();
            if (isValidRotationAngle(rotationAngle)) {
                mRotationAngle = rotationAngle;
            }
        }catch (Exception e){}

//            a.recycle();
//        }
        setTouchEventListener(new TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                MmiPoint point = touchEvent.getPointerPosition(touchEvent.getIndex());
                float x = point.getX();
                float y = point.getY();
                System.out.println("x= " + x);
                System.out.println("y= " + y);
                switch (touchEvent.getAction()) {
                    case TouchEvent.PRIMARY_POINT_DOWN:
                    case TouchEvent.PRIMARY_POINT_UP:
                    case TouchEvent.POINT_MOVE:
                        if (mRotationAngle == ROTATION_ANGLE_CW_270) {
                            //从下到上
                            System.out.println("=setProgressValue== " + (getMax() - (int) (getMax() * y / getHeight())));
                            setProgressValue(getMax() - (int) (getMax() * y / getHeight()));
                        } else if (mRotationAngle == ROTATION_ANGLE_CW_90) {
                            //从上到下
                            setProgressValue((int) (getMax() * y / getHeight()));
                        }
//                        onSizeChanged(getWidth(), getHeight(), 0, 0);
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
        invalidate();
    }

//    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
//        super.onSizeChanged(h, w, oldh, oldw);
//    }

//    @Override
//    protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(heightMeasureSpec, widthMeasureSpec);
//        setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
//    }

    @Override
    public void invalidate() {
        super.invalidate();
        addDrawTask(this::onDraw);
    }

    @Override
    public void onDraw(Component component, Canvas c) {
        System.out.println("============onDrawonDraw"+mRotationAngle);
        if (mRotationAngle == ROTATION_ANGLE_CW_270) {
            System.out.println("============onDraw270");
            //从下到上
            c.rotate(270,0,0);
            c.translate(-getHeight(), 0);
            System.out.println("============onDraw"+-getHeight());
        } else if (mRotationAngle == ROTATION_ANGLE_CW_90) {
            //从上到下
            c.rotate(90,0,0);
            c.translate(0, -getWidth());
        }
    }



    private static boolean isValidRotationAngle(int angle) {
        return (angle == ROTATION_ANGLE_CW_90 || angle == ROTATION_ANGLE_CW_270);
    }
}
