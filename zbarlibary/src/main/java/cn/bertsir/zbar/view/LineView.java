package cn.bertsir.zbar.view;


import cn.bertsir.zbar.ResourceTable;
import ohos.agp.colors.RgbPalette;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Shader;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Point;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

/**
 * Created by Bert on 2017/9/22.
 */

@Deprecated
public class LineView extends Component implements Component.DrawTask {
    private int line_color;
    private Paint paint;//声明画笔
    private Canvas canvas;//画布
    private Shader mShader;

    public LineView(Context context) {
        super(context);
    }

    public LineView(Context context, AttrSet attrs) {
        super(context, attrs);
        try {
            line_color = getResourceManager().getElement(ResourceTable.Color_common_color).getColor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        paint=new Paint();//创建一个画笔
        canvas=new Canvas();
        paint.setStyle(Paint.Style.FILL_STYLE);//设置非填充
        paint.setStrokeWidth(10);//笔宽5像素
        paint.setColor(new Color(line_color));//设置为红笔
        paint.setAntiAlias(true);//锯齿不显示
        invalidate();
    }

    @Override
    public void invalidate() {
        super.invalidate();
        addDrawTask(this::onDraw);
    }

    public void setLinecolor(int color){
        line_color = color;
        invalidate();
    }

    @Override
    public void onDraw(Component component,Canvas canvas) {
        String line_colors = String.valueOf(line_color);
        line_colors = line_colors.substring(line_colors.length() - 6, line_colors.length() - 0);
        mShader =  new Shader(new Color[]{new Color(RgbPalette.parse("#00" + line_colors)), new Color(line_color), new Color(RgbPalette.parse("#00" + line_colors))},
                Shader.TileMode.CLAMP_TILEMODE) {
            @Override
            public void setShaderMatrix(Matrix matrix) {
                super.setShaderMatrix(matrix);
            }
        };
        paint.setShader(mShader, Paint.ShaderType.LINEAR_SHADER);
        canvas.drawLine(new Point(0, 0), new Point(getWidth(), 0), paint);
    }


}
