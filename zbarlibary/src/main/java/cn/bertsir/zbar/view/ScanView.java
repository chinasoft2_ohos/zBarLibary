package cn.bertsir.zbar.view;


import cn.bertsir.zbar.QrConfig;
import cn.bertsir.zbar.ResourceTable;
import ohos.agp.components.*;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.Optional;


/**
 * Created by Bert on 2017/9/20.
 */

public class ScanView extends StackLayout {

    private ScanLineView iv_scan_line;
    private StackLayout fl_scan;
    private int CURRENT_TYEP = 1;
    private CornerView cnv_left_top;
    private CornerView cnv_left_bottom;
    private CornerView cnv_right_top;
    private CornerView cnv_right_bottom;
    private ArrayList<CornerView> cornerViews;
    private int line_speed = 3000;

    public ScanView(Context context) {
        super(context);
        initView(context);
    }

    public ScanView(Context context, AttrSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public ScanView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context mContext) {
        Component scan_view = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_view_scan, this, true);
        cnv_left_top = (CornerView) scan_view.findComponentById(ResourceTable.Id_cnv_left_top);
        cnv_left_bottom = (CornerView) scan_view.findComponentById(ResourceTable.Id_cnv_left_bottom);
        cnv_right_top = (CornerView) scan_view.findComponentById(ResourceTable.Id_cnv_right_top);
        cnv_right_bottom = (CornerView) scan_view.findComponentById(ResourceTable.Id_cnv_right_bottom);

        cornerViews = new ArrayList<>();
        cornerViews.add(cnv_left_top);
        cornerViews.add(cnv_left_bottom);
        cornerViews.add(cnv_right_top);
        cornerViews.add(cnv_right_bottom);

        iv_scan_line = (ScanLineView) scan_view.findComponentById(ResourceTable.Id_iv_scan_line);

        fl_scan = (StackLayout) scan_view.findComponentById(ResourceTable.Id_fl_scan);
//        getViewWidthHeight();
        invalidate();
    }

    /**
     * 设置扫描速度
     *
     * @param speed
     */
    public void setLineSpeed(int speed) {
        iv_scan_line.setScanAnimatorDuration(speed);
    }


    /**
     * 设置扫描样式
     * @param  style 样式
     */
    public void setScanLineStyle(int style) {
        iv_scan_line.setScanStyle(style);
    }


    public void setType(int type) {
        CURRENT_TYEP = type;
        ComponentContainer.LayoutConfig fl_params = fl_scan.getLayoutConfig();
        if (CURRENT_TYEP == QrConfig.SCANVIEW_TYPE_QRCODE) {
            fl_params.width = dip2px(200);
            fl_params.height = dip2px(200);
        } else if (CURRENT_TYEP == QrConfig.SCANVIEW_TYPE_BARCODE) {
            fl_params.width = dip2px(300);
            fl_params.height = dip2px(100);
        }
        fl_scan.setLayoutConfig(fl_params);
    }

    public void setCornerColor(int color) {
        for (int i = 0; i < cornerViews.size(); i++) {
            cornerViews.get(i).setColor(color);
        }
    }

    public void setCornerWidth(int dp) {
        for (int i = 0; i < cornerViews.size(); i++) {
            cornerViews.get(i).setLineWidth(dp);
        }
    }

    public void setLineColor(int color) {
        iv_scan_line.setScancolor(color);
    }

    public int dip2px(int dp) {
        DisplayAttributes displayAttributes = getScreenPiex(getContext());
        return (int) (dp * displayAttributes.scalDensity);
    }

    public DisplayAttributes getScreenPiex(Context context) {
        // 获取屏幕密度
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = display.get().getAttributes();
        //displayAttributes.xDpi;
        //displayAttributes.yDpi
        return displayAttributes;
    }

//    public void getViewWidthHeight() {
//        Symbol.cropWidth = fl_scan.getWidth();
//        Symbol.cropHeight = fl_scan.getHeight();
//    }


}
