package cn.bertsir.zbar.view;


import ohos.agp.colors.RgbPalette;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.Optional;

/**
 * Created by Bert on 2017/9/22.
 */

public class CornerView extends Component implements Component.DrawTask {

    private Paint paint;//声明画笔
    private Canvas canvas;//画布

    private static final String TAG = "CornerView";
    private int width = 0;
    private int height = 0;

    private int cornerColor= RgbPalette.parse("#ff5f00");
    private int cornerWidth=5;
    private int cornerGravity;


    private static final int LEFT_TOP = 0;
    private static final int LEFT_BOTTOM = 1;
    private static final int RIGHT_TOP = 2;
    private static final int RIGHT_BOTTOM = 3;

    public CornerView(Context context) {
        super(context,null);
    }

    public CornerView(Context context,  AttrSet attrs) {
        super(context, attrs);
//        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CornerView);
//        cornerColor = a.getColor(R.styleable.CornerView_corner_color, getResources().getColor(R.color.common_color));
//        cornerWidth = (int) a.getDimension(R.styleable.CornerView_corner_width, 10);
//        cornerGravity = a.getInt(R.styleable.CornerView_corner_gravity, 1);
//        a.recycle();
        cornerGravity=attrs.getAttr("corner_gravity").get().getIntegerValue();
        paint=new Paint();//创建一个画笔
        canvas=new Canvas();

        paint.setStyle(Paint.Style.FILL_STYLE);//设置非填充
        paint.setStrokeWidth(cornerWidth);//笔宽5像素
        paint.setColor(new Color(cornerColor));//设置为红笔
        paint.setAntiAlias(true);//锯齿不显示
        width = 20;
        height = 20;
        invalidate();
    }

    @Override
    public void invalidate() {
        super.invalidate();
        addDrawTask(this::onDraw);
    }
    @Override
    public void onDraw(Component component, Canvas canvas) {
        switch (cornerGravity){
            case LEFT_TOP:
                canvas.drawLine(new Point(0,0), new Point(width,0), paint);
                canvas.drawLine(new Point(0,0), new Point(0, height), paint);
                break;
            case LEFT_BOTTOM:
                canvas.drawLine(new Point(0,0), new Point(0, height), paint);
                canvas.drawLine(new Point(0,height), new Point(width, height), paint);
                break;
            case RIGHT_TOP:
                canvas.drawLine(new Point(0,0), new Point(width, 0), paint);
                canvas.drawLine(new Point(width,0), new Point(width, height), paint);
                break;
            case RIGHT_BOTTOM:
                canvas.drawLine(new Point(width,0), new Point(width, height), paint);
                canvas.drawLine(new Point(0,height), new Point(width, height), paint);
                break;
        }

    }


    public void setColor(int color){
        cornerColor = color;
        paint.setColor(new Color(cornerColor));
        invalidate();
    }

    public void setLineWidth(int dp){
        cornerWidth = dip2px(getContext(),dp);
        paint.setStrokeWidth(cornerWidth);
        invalidate();
    }

    public DisplayAttributes getScreenPiex(Context context){
        // 获取屏幕密度
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = display.get().getAttributes();
        //displayAttributes.xDpi;
        //displayAttributes.yDpi
        return  displayAttributes;
    }

    public int dip2px(Context context, int dp){
        // 获取屏幕密度
        DisplayAttributes displayAttributes = getScreenPiex(context);
        return  (int)(dp * displayAttributes.scalDensity);
    }
//    public int dip2px(int dp) {
//        float density = getContext().getResources().getDisplayMetrics().density;
//        return (int) (dp * density + 0.5);
//    }




}
