/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.bertsir.zbar.utils;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * 打印日志
 *
 * @since 2021-04-21
 */
public class Log {
    /**
     * debug
     *
     * @param tag 5tag
     * @param message msg
     */
    public static void debug(String tag, String message) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0, tag);
        HiLog.debug(label, message);
    }

    /**
     * debug
     *
     * @param message msg
     */
    public static void debug(String message) {
        debug("LogMessage", message);
    }

    /**
     * error
     *
     * @param tag tag
     * @param message mag
     */
    public static void error(String tag, String message) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0, tag);
        HiLog.error(label, message);
    }



    /**
     * error
     *
     * @param message msg
     */
    public static void error(String message) {
        error("LogMessage", message);
    }
}
