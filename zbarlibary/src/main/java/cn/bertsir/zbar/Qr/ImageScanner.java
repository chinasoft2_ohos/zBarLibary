/*------------------------------------------------------------------------
 *  ImageScanner
 *
 *  Copyright 2007-2010 (c) Jeff Brown <spadix@users.sourceforge.net>
 *
 *  This file is part of the ZBar Bar Code Reader.
 *
 *  The ZBar Bar Code Reader is free software; you can redistribute it
 *  and/or modify it under the terms of the GNU Lesser Public License as
 *  published by the Free Software Foundation; either version 2.1 of
 *  the License, or (at your option) any later version.
 *
 *  The ZBar Bar Code Reader is distributed in the hope that it will be
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 *  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser Public License
 *  along with the ZBar Bar Code Reader; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 *  Boston, MA  02110-1301  USA
 *
 *  http://sourceforge.net/projects/zbar
 *------------------------------------------------------------------------*/

package cn.bertsir.zbar.Qr;

/**
 * Read barcodes from 2-D images.
 */
@SuppressWarnings("JniMissingFunction")
public class ImageScanner {
    /**
     * C pointer to a zbar_image_scanner_t.
     */
    private long peer;

    static {
        System.loadLibrary("zbar");
        init();
    }

    private static native void init();

    public ImageScanner() {
        peer = create();
    }


    private native long create();

    protected void finalize() {
        destroy();
    }

    /**
     * Clean up native data associated with an instance.
     */
    public synchronized void destroy() {
        if (peer != 0) {
            destroy(peer);
            peer = 0;
        }
    }


    private native void destroy(long peer);


    public native void setConfig(int symbology, int config, int value)
            throws IllegalArgumentException;


    public native void parseConfig(String config);


    public native void enableCache(boolean enable);


    public SymbolSet getResults() {
        return (new SymbolSet(getResults(peer)));
    }

    private native long getResults(long peer);


    public native int scanImage(Image image);
}
