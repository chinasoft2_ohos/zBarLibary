/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.bertsir.zbar.decoding;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;

import java.util.*;
import java.util.regex.Pattern;

import static java.util.Collections.singletonList;

/**
 * 解码类型格式
 */
public class DecodeFormatManager {


    private DecodeFormatManager() {}

    /**
     * 所有的
     */
    public static final Map<DecodeHintType,Object> ALL_HINTS = new EnumMap<>(DecodeHintType.class);
    /**
     * CODE_128 (最常用的一维码)
     */
    public static final Map<DecodeHintType,Object> CODE_128_HINTS = createDecodeHint(BarcodeFormat.CODE_128);
    /**
     * QR_CODE (最常用的二维码)
     */
    public static final Map<DecodeHintType,Object> QR_CODE_HINTS = createDecodeHint(BarcodeFormat.QR_CODE);
    /**
     * 一维码
     */
    public static final Map<DecodeHintType,Object> ONE_DIMENSIONAL_HINTS = new EnumMap<>(DecodeHintType.class);
    /**
     * 二维码
     */
    public static final Map<DecodeHintType,Object> TWO_DIMENSIONAL_HINTS = new EnumMap<>(DecodeHintType.class);

    /**
     * 默认
     */
    public static final Map<DecodeHintType,Object> DEFAULT_HINTS = new EnumMap<>(DecodeHintType.class);

    static {
        //all hints
        addDecodeHintTypes(ALL_HINTS,getAllFormats());
        //one dimension
        addDecodeHintTypes(ONE_DIMENSIONAL_HINTS,getOneDimensionalFormats());
        //Two dimension
        addDecodeHintTypes(TWO_DIMENSIONAL_HINTS,getTwoDimensionalFormats());
    }

    /**
     * 所有支持的{@link BarcodeFormat}
     * @return List<BarcodeFormat>
     */
    private static List<BarcodeFormat> getAllFormats(){
        List<BarcodeFormat> list = new ArrayList<>();
        list.add(BarcodeFormat.AZTEC);
        list.add(BarcodeFormat.CODABAR);
        list.add(BarcodeFormat.CODE_39);
        list.add(BarcodeFormat.CODE_93);
        list.add(BarcodeFormat.CODE_128);
        list.add(BarcodeFormat.DATA_MATRIX);
        list.add(BarcodeFormat.EAN_8);
        list.add(BarcodeFormat.EAN_13);
        list.add(BarcodeFormat.ITF);
        list.add(BarcodeFormat.MAXICODE);
        list.add(BarcodeFormat.PDF_417);
        list.add(BarcodeFormat.QR_CODE);
        list.add(BarcodeFormat.RSS_14);
        list.add(BarcodeFormat.RSS_EXPANDED);
        list.add(BarcodeFormat.UPC_A);
        list.add(BarcodeFormat.UPC_E);
        list.add(BarcodeFormat.UPC_EAN_EXTENSION);
        return list;
    }

    /**
     * 二维码
     * 包括如下几种格式：
     *  {@link BarcodeFormat#CODABAR}
     *  {@link BarcodeFormat#CODE_39}
     *  {@link BarcodeFormat#CODE_93}
     *  {@link BarcodeFormat#CODE_128}
     *  {@link BarcodeFormat#EAN_8}
     *  {@link BarcodeFormat#EAN_13}
     *  {@link BarcodeFormat#ITF}
     *  {@link BarcodeFormat#RSS_14}
     *  {@link BarcodeFormat#RSS_EXPANDED}
     *  {@link BarcodeFormat#UPC_A}
     *  {@link BarcodeFormat#UPC_E}
     *  {@link BarcodeFormat#UPC_EAN_EXTENSION}
     * @return List<BarcodeFormat>
     */
    private static List<BarcodeFormat> getOneDimensionalFormats(){
        List<BarcodeFormat> list = new ArrayList<>();
        list.add(BarcodeFormat.CODABAR);
        list.add(BarcodeFormat.CODE_39);
        list.add(BarcodeFormat.CODE_93);
        list.add(BarcodeFormat.CODE_128);
        list.add(BarcodeFormat.EAN_8);
        list.add(BarcodeFormat.EAN_13);
        list.add(BarcodeFormat.ITF);
        list.add(BarcodeFormat.RSS_14);
        list.add(BarcodeFormat.RSS_EXPANDED);
        list.add(BarcodeFormat.UPC_A);
        list.add(BarcodeFormat.UPC_E);
        list.add(BarcodeFormat.UPC_EAN_EXTENSION);
        return list;
    }

    /**
     * 二维码
     * 包括如下几种格式：
     *  {@link BarcodeFormat#AZTEC}
     *  {@link BarcodeFormat#DATA_MATRIX}
     *  {@link BarcodeFormat#MAXICODE}
     *  {@link BarcodeFormat#PDF_417}
     *  {@link BarcodeFormat#QR_CODE}
     * @return List<BarcodeFormat>
     */
    private static List<BarcodeFormat> getTwoDimensionalFormats(){
        List<BarcodeFormat> list = new ArrayList<>();
        list.add(BarcodeFormat.AZTEC);
        list.add(BarcodeFormat.DATA_MATRIX);
        list.add(BarcodeFormat.MAXICODE);
        list.add(BarcodeFormat.PDF_417);
        list.add(BarcodeFormat.QR_CODE);
        return list;
    }
    /**
     * 支持解码的格式
     * @param barcodeFormats {@link BarcodeFormat}
     * @return Map<DecodeHintType,Object>
     */
    public static Map<DecodeHintType,Object> createDecodeHints( BarcodeFormat... barcodeFormats){
        Map<DecodeHintType,Object> hints = new EnumMap<>(DecodeHintType.class);
        addDecodeHintTypes(hints, Arrays.asList(barcodeFormats));
        return hints;
    }

    /**
     * 支持解码的格式
     * @param barcodeFormat {@link BarcodeFormat}
     * @return Map<DecodeHintType,Object>
     */
    public static Map<DecodeHintType,Object> createDecodeHint( BarcodeFormat barcodeFormat){
        Map<DecodeHintType,Object> hints = new EnumMap<>(DecodeHintType.class);
        addDecodeHintTypes(hints,singletonList(barcodeFormat));
        return hints;
    }
    private static void addDecodeHintTypes(Map<DecodeHintType,Object> hints, List<BarcodeFormat> formats){
        // Image is known to be of one of a few possible formats.
        hints.put(DecodeHintType.POSSIBLE_FORMATS, formats);
        // Spend more time to try to find a barcode; optimize for accuracy, not speed.
        hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
        // Specifies what character encoding to use when decoding, where applicable (type String)
        hints.put(DecodeHintType.CHARACTER_SET, "UTF-8");
    }
}
