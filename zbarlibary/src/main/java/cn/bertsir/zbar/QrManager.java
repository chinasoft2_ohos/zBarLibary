package cn.bertsir.zbar;


import cn.bertsir.zbar.Qr.ScanResult;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.app.Context;

/**
 * Created by Bert on 2017/9/22.
 */

public class QrManager {

    private static QrManager instance;
    private QrConfig options;

    public OnScanResultCallback resultCallback;

    public synchronized static QrManager getInstance() {
        if(instance == null)
            instance = new QrManager();
        return instance;
    }

    public OnScanResultCallback getResultCallback() {
        return resultCallback;
    }


    public QrManager init(QrConfig options) {
        this.options = options;
        return this;
    }

    public void startScan(final Context context, OnScanResultCallback resultCall){
        if (options == null) {
            options = new QrConfig.Builder().create();
        }
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(context.getBundleName())
                .withAbilityName("cn.bertsir.zbar.QRActivity")
                .build();
        intent.setParam(QrConfig.EXTRA_THIS_CONFIG, options);
        intent.setOperation(operation);
        context.startAbility(intent,0);
        // 绑定回调函数事件
        resultCallback = resultCall;
    }



    public interface OnScanResultCallback {
        /**
         * 处理成功
         * 多选
         *
         * @param result
         */
        void onScanSuccess(ScanResult result);
    }
}
