
package cn.bertsir.qrtest;

import cn.bertsir.qrtest.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.bundle.IBundleManager;

public class MainAbility extends Ability {
    private String permissionStorage = "ohos.permission.READ_USER_STORAGE";
    private String permissionCamera = "ohos.permission.CAMERA";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        String[] permissions = {permissionStorage, permissionCamera};
        if (verifySelfPermission(permissionCamera) != IBundleManager.PERMISSION_GRANTED
                && verifySelfPermission(permissionStorage) != IBundleManager.PERMISSION_GRANTED
        ) {
            // 应用未被授予权限
            if (canRequestPermission(permissionCamera)
                    && canRequestPermission(permissionStorage)) {
                // 是否可以申请弹框授权(首次申请或者用户未选择禁止且不再提示)
                requestPermissionsFromUser(
                        permissions, 1);
            }
        }
    }
}
