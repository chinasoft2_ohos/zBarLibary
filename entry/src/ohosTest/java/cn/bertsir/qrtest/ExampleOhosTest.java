/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.bertsir.qrtest;


import cn.bertsir.qrtest.slice.MainAbilitySlice;
import cn.bertsir.zbar.Qr.ScanResult;
import cn.bertsir.zbar.QrConfig;
import cn.bertsir.zbar.QrManager;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.colors.RgbPalette;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ExampleOhosTest {
    private static QrConfig options;

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("cn.bertsir.qrtest", actualBundleName);
    }

    @BeforeClass
    public static void set() {
        Ability ability = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        options = new QrConfig.Builder()
                .setDesText("扫描框下文字")// 扫描框下文字
                .setShowDes(true)// 是否显示扫描框下面文字
                .setShowLight(true)// 显示手电筒按钮
                .setShowTitle(true)// 显示Title
                .setShowAlbum(true)// 显示从相册选择按钮
                .setScanType(1)// 设置扫码类型（二维码，条形码，全部，自定义，默认为二维码）
                .setScanViewType(1)// 设置扫描框类型（二维码还是条形码，默认为二维码）
                .setPlaySound(true)// 是否扫描成功后bi~的声音
                .setTitleText("测试标题")// 设置Tilte文字
                .setTitleTextColor(Color.WHITE.getValue())// 设置Title文字颜色
                .setLooperScan(true)// 是否连续扫描二维码
                .setLooperWaitTime(5 * 1000)// 连续扫描间隔时间
                .setShowVibrator(true)// 是否震动提醒
                .create();
    }

    @Test
    public void getLoop_wait_time() {
        assertTrue(5000 == options.getLoop_wait_time());
    }

    @Test
    public void getScan_type() {
        assertTrue(1 == options.getScan_type());
    }

    @Test
    public void getTitle_text() {
        assertTrue("测试标题".equals(options.getTitle_text()));
    }

    @Test
    public void getScan_view_type() {
        assertTrue(1 == options.getScan_view_type());
    }

    @Test
    public void isPlay_sound() {
        assertTrue(options.isPlay_sound());
    }

    @Test
    public void isShow_light() {
        assertTrue(options.isShow_light());
    }

    @Test
    public void isShow_album() {
        assertTrue(options.isShow_album());
    }

    @Test
    public void isShow_title() {
        assertTrue(options.isShow_title());
    }

    @Test
    public void isLoop_scan() {
        assertTrue(options.isLoop_scan());
    }

    @Test
    public void isShow_vibrator() {
        assertTrue(options.isShow_vibrator());
    }
}