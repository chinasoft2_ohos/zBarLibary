# zBarLibary


#### 项目介绍
- 项目名称：zBarLibary二维码扫描控件
- 所属系列：openharmony的第三方组件适配移植
- 功能：实现二维码生成、扫描
- 项目移植状态：主功能完成，部分功能目前openharmonySDK不支持
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本： Release  1.4.2

#### 效果演示

<img src="img/1.gif"></img>

#### 安装教程
1.在项目根目录下的build.gradle文件中，
 ```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:zBarLibary:1.0.0')
    ......
 }
 ```
在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
## 1.识别二维码
<pre>

        QrConfig qrConfig = new QrConfig.Builder()
                .setDesText("(识别二维码)")//扫描框下文字
                .setShowDes(false)//是否显示扫描框下面文字
                .setShowLight(true)//显示手电筒按钮
                .setShowTitle(true)//显示Title
                .setShowAlbum(true)//显示从相册选择按钮
                .setCornerColor(Color.WHITE)//设置扫描框颜色
                .setLineColor(Color.WHITE)//设置扫描线颜色
                .setLineSpeed(QrConfig.LINE_MEDIUM)//设置扫描线速度
                .setScanType(QrConfig.TYPE_QRCODE)//设置扫码类型（二维码，条形码，全部，自定义，默认为二维码）
                .setScanViewType(QrConfig.SCANVIEW_TYPE_QRCODE)//设置扫描框类型（二维码还是条形码，默认为二维码）
                .setCustombarcodeformat(QrConfig.BARCODE_I25)//此项只有在扫码类型为TYPE_CUSTOM时才有效
                .setPlaySound(true)//是否扫描成功后bi~的声音
                .setDingPath("resources/rawfile/test.mp3")//设置提示音(不设置为默认的Ding~)
                .setIsOnlyCenter(true)//是否只识别框中内容(默认为全屏识别)
                .setTitleText("扫描二维码")//设置Tilte文字
                .setTitleBackgroudColor(Color.BLUE)//设置状态栏颜色
                .setTitleTextColor(Color.BLACK)//设置Title文字颜色
                .setShowZoom(false)//是否手动调整焦距
                .setAutoZoom(false)//是否自动调整焦距
                .setFingerZoom(false)//是否开始双指缩放
                .setScreenOrientation(QrConfig.SCREEN_PORTRAIT)//设置屏幕方向
                .setDoubleEngine(false)//是否开启双引擎识别(仅对识别二维码有效，并且开启后只识别框内功能将失效)
                .setLooperScan(false)//是否连续扫描二维码
                .setLooperWaitTime(5*1000)//连续扫描间隔时间
                .setScanLineStyle(ScanLineView.style_radar)//扫描动画样式
                .setAutoLight(false)//自动灯光
                .setShowVibrator(false)//是否震动提醒
                .create();
  QrManager.getInstance().init(qrConfig).startScan(MainAbilitySlice.this, new QrManager.OnScanResultCallback() {
            @Override
            public void onScanSuccess(ScanResult rs) {
                 System.out.println(rs.getContent());
            }
        });
</pre>

##### 具体使用可参考Demo

## 2.生成码
###  2.1生成二维码
<pre>
PixelMap qrCode = QRUtils.getInstance().createQRCode("www.qq.com");
</pre>

####  2.1.1生成二维码并添加Logo
<pre>
try {
                        ResourceManager manager = getResourceManager();
                        String mediaPath = getResourceManager().getMediaPath(ResourceTable.Media_icon);
                        Resource resource1 = null;
                        resource1 = manager.getRawFileEntry(mediaPath).openRawFile();
                        ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
                        ImageSource imageSource = ImageSource.create(resource1, sourceOptions);
                        PixelMap pixelMap = imageSource.createPixelmap(null);
                        qrCode = QRUtils.getInstance().createQRCodeAddLogo(et_qr_content.getText().toString(),
                                pixelMap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (NotExistException e) {
                        e.printStackTrace();
                    } catch (WrongTypeException e) {
                        e.printStackTrace();
                    }
</pre>

## 3.识别本地
### 3.1 识别本地二维码
<pre>

String s = QRUtils.getInstance().decodeQRcode(Image);
String s1 = QRUtils.getInstance().decodeQRcodeByZxing(PixelMap);
</pre>


## 4.参数描述
| name | format | description |
| ------------- |:-------------:| :-------------:|
| setDesText | String | 设置扫描框下方描述文字 |
| setShowDes | Boolean | 设置是否显示扫描框下方描述文字 |
| setShowLight | Boolean | 是否开启手电筒功能 |
| setShowAlbum | Boolean | 是否开启从相册选择功能 |
| setShowTitle | Boolean | 是否显示Title |
| setTitleText | String | 设置Title文字 |
| setTitleBackgroudColor | int | 设置Title背景色 |
| setTitleTextColor | int | 设置Title文字颜色 |
| setCornerColor | int | 设置扫描框颜色 |
| setLineColor | int | 设置扫描线颜色 |
| setLineSpeed | int | 设置扫描线速度</br>QrConfig.LINE_FAST(快速)</br>QrConfig.LINE_MEDIUM(中速）<br>QrConfig.LINE_SLOW(慢速) <br>也可以自定义时间(单位毫秒)|
| setScanType | int | 设置扫描类型</br>QrConfig.TYPE_QRCODE(二维码) |
| setScanViewType | int | 设置扫描框类型</br>QrConfig.SCANVIEW_TYPE_QRCODE(二维码)</br>QrConfig.SCANVIEW_TYPE_BARCODE(条形码) |
| setCustombarcodeformat| int| 设置指定扫码类型（举例：QrConfig.BARCODE_EAN13）,此项只有在ScanType设置为自定义时才生效 |
| setIsOnlyCenter| Boolean | 设置是否只识别扫描框中的内容（默认为全屏扫描） |
| setPlaySound | Boolean | 设置扫描成功后是否有提示音 |
| setDingPath | int| 自定义提示音（举例：resources/rawfile/test.mp3，不设置为默认的) |
| setNeedCrop | Boolean | 从相册选择二维码之后再次手动框选二维码(默认为true) |
| setShowZoom | Boolean | 是否开启手动调整焦距(默认为false) |
| setAutoZoom | Boolean | 是否开启自动调整焦距(默认为false) |
| setFingerZoom | Boolean | 是否开启双指调整焦距(默认为false) |
| setScreenOrientation | int | 设置屏幕方向</br>QrConfig.SCREEN_PORTRAIT(纵向)</br>QrConfig.SCREEN_LANDSCAPE(横向）<br>QrConfig.SCREEN_SENSOR(传感器方向) |
| setDoubleEngine | Boolean | 是否开启双识别引擎(默认为false) |
| setLooperScan | Boolean | 是否开启连续扫描(默认为false) |
| setOpenAlbumText | String | 设置打开相册的文字 |
| setLooperWaitTime | int | 设置连续扫描间隔时间，单位毫秒（默认为0） |
| setScanLineStyle | int | 设置扫描动画样式</br>ScanLineView.style_radar(雷达)</br>ScanLineView.style_gridding(网格）<br>ScanLineView.style_hybrid(网格+雷达) <br>ScanLineView.style_line(线条)（默认为线条） |
| setAutoLight | Boolean | 是否开启自动灯光(默认为false)|
| setShowVibrator | Boolean | 是否开启震动提醒(默认为false)|

#### 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

- 1.0.0

#### 版权和许可信息
 ```
MIT License

Copyright (c) 2018 bertsir

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 ```